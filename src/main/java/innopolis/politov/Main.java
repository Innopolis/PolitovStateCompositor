package innopolis.politov;

/**
 * Created by General on 3/8/2017.
 */
public class Main {

    public static void main(String[] args) {
        House house = new House(13L);
        House house1 = new House(20L);

        Quarter quarter = new Quarter(1000L);
        quarter.add(house);
        quarter.add(house1);

        District district = new District();
        district.add(quarter);
        district.add(quarter);
        district.add(quarter);

        City city = new City(100000L);
        city.add(district);
        city.add(district);

        Federation federation = new Federation();
        federation.add(city);
        federation.add(city);

        Country country = new Country();
        country.add(federation);
        country.add(federation);

        System.out.println("Budget = " + country.sumBudget());
        System.out.println("People count = " + country.sumPeople());
        System.out.println("Square = " + country.sumSquare());
    }






}
