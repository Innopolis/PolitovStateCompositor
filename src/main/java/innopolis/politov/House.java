package innopolis.politov;

/**
 * Created by General on 3/8/2017.
 */
public class House extends StateComponent{

    protected boolean isComposite() {
        return false;
    }

    public House(Long countPeople) {
        super(countPeople, 0L, 0L);
    }
}
