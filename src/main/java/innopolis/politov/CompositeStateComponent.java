package innopolis.politov;

/**
 * Created by General on 3/8/2017.
 */
public class CompositeStateComponent extends StateComponent{
    public CompositeStateComponent(Long countPeople, Long square, Long budget) {
        super(countPeople, square, budget);
    }

    protected boolean isComposite() {
        return true;
    }
}
