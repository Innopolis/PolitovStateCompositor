package innopolis.politov;

import java.util.ArrayList;

/**
 * Created by General on 3/8/2017.
 */
public abstract class StateComponent {
    private StateComponent parentComponent;

    private Long countPeople;
    private Long square;
    private Long budget;

    private ArrayList<StateComponent> childs;

    protected abstract boolean isComposite();

    public Long sumPeople(){
        Long sum = 0L;
        if (isComposite()) {
            for (StateComponent component :
                    childs) {
                sum += component.sumPeople();
            }
        }
        return sum+= countPeople;
    }
    public Long sumSquare(){
        Long sum = 0L;
        if (isComposite()) {
            for (StateComponent component :
                    childs) {
                sum += component.sumSquare();
            }
        }
        return sum+= square;
    }
    public Long sumBudget(){
        Long sum = 0L;
        if (isComposite()) {
            for (StateComponent component :
                    childs) {
                sum += component.sumBudget();
            }
        }
        return sum+= budget;
    }

    public StateComponent(Long countPeople, Long square, Long budget) {
        this.countPeople = countPeople;
        this.square = square;
        this.budget = budget;
        this.childs = new  ArrayList<StateComponent>();
    }

    public void add(StateComponent component){
        childs.add(component);
    }
    public void remove(StateComponent component){
        childs.remove(component);
    }
    public ArrayList<StateComponent> getChild(){
        return childs;
    }
}
